package com.example.anafulus_agent;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Login extends AppCompatActivity {

    private Toolbar toolbar;
    private Button buttonLogin, buttonLoginPhone, buttonLoginGoogle;
    private TextView textViewRegister, textViewReset;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Masuk");

        textViewReset = findViewById(R.id.forgot);
        textViewReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Forgot.class);
                startActivity(i);
            }
        });

//        // login
//        buttonLoginPhone = findViewById(R.id.buttonLoginPhone);
//        buttonLoginPhone.setTypeface(typeface);
//        buttonLoginPhone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Login.this, LoginPonsel.class);
//                startActivity(intent);
//            }
//        });
//
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setTypeface(typeface);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Dashboard.class);
                startActivity(intent);
            }
        });
//
//        buttonLoginGoogle = findViewById(R.id.buttonLoginGoogle);
//        buttonLoginGoogle.setTypeface(typeface);
//        buttonLoginGoogle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Login.this, LoginGoogle.class);
//                startActivity(intent);
//            }
//        });
//
//        // register
//        textViewRegister = findViewById(R.id.textViewRegister);
//        textViewRegister.setTypeface(typeface);
//        textViewRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Login.this, Register.class);
//                startActivity(intent);
//            }
//        });

    }
}
