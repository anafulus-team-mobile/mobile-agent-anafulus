package com.example.anafulus_agent;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ViewFlipper;

public class Dashboard extends AppCompatActivity {

    ViewFlipper viewFlipper;

    private static final String TAG = "PageDashboard";
    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;
    private Button logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_dashboard);

//        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

//        tabLayout.getTabAt(0).setText("Reguler");
//        tabLayout.getTabAt(1).setText("Murabahah");

        viewFlipper = findViewById(R.id.v_flipper);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_dashboard:
                        finish();
                        break;

                    case R.id.nav_notify:
                        Intent intent1 = new Intent(Dashboard.this, Notification.class);
                        startActivity(intent1);
                        finish();
                        break;

                    case R.id.nav_history:
                        Intent intent2 = new Intent(Dashboard.this, Information.class);
                        startActivity(intent2);
                        finish();
                        break;

                    case R.id.nav_account:
                        Intent intent3 = new Intent(Dashboard.this, History.class);
                        startActivity(intent3);
                        finish();
                        break;
                }


                return false;
            }
        });

//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LoginManager.getInstance().logOut();
//                Intent i = new Intent(PageDashboard.this, Login.class);
//                startActivity(i);
//                finish();
//            }
//        });

    }
    private void setupViewPager(ViewPager viewPager) {
//        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
//        adapter.addFragment(new Tab1Fragment());
//        adapter.addFragment(new Tab2Fragment());
//        viewPager.setAdapter(adapter);
    }
}