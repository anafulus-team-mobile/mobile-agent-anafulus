package com.example.anafulus_agent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;


public class FormRegisterEmail extends AppCompatActivity {


    private EditText Name, Number, Password, Cpassword;
    private TextView email;
    private CheckBox check;
    private Button btn_regist;
    String string;
    Toolbar toolbar;
    AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_register_email);

        Name = findViewById(R.id.name);
        Number = findViewById(R.id.number);
        Password = findViewById(R.id.password);
        Cpassword = findViewById(R.id.cpassword);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        email = findViewById(R.id.email);
        string = getIntent().getExtras().getString("email");
        email.setText(string);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FormRegisterEmail.this, Register.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
        Registrasi();
    }
    private void Registrasi() {
        btn_regist = findViewById(R.id.btn_regist);
        String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{8,}";

        awesomeValidation.addValidation(FormRegisterEmail.this, R.id.name, "[a-zA-Z\\s]+",R.string.RegisterName);
        awesomeValidation.addValidation(FormRegisterEmail.this, R.id.number, RegexTemplate.TELEPHONE,R.string.RegisterPhone);
//        awesomeValidation.addValidation(DataUserEmail.this, R.id.password, regexPassword, R.string.RegisterPass);
        awesomeValidation.addValidation(FormRegisterEmail.this,R.id.cpassword,R.id.password,R.string.RegisterCPass);

        btn_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mPassword = Password.getText().toString().trim();
                String mNumber = Number.getText().toString().trim();
                Registrasi();
                if (awesomeValidation.validate() && (!mPassword.isEmpty() && (!mNumber.isEmpty()) )){
                    Intent i = new Intent(FormRegisterEmail.this, Login.class);
                    Toast.makeText(FormRegisterEmail.this, "Berhasil Daftar, Silahkan Masuk", Toast.LENGTH_SHORT).show();
                    startActivity(i);
                    finish();
                } else {
                    Password.setError("Harap Masukkan Kata Sandi Anda");
                    Number.setError("Masukkan Nomor Ponsel Anda");
                }

            }
        });
    }
}