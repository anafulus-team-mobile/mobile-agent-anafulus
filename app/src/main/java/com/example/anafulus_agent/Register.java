package com.example.anafulus_agent;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Register extends AppCompatActivity {
    private Button btn_regist, btn_fb, btn_hp;
    private EditText email, password;
    private Toolbar toolbar;
    private TextView tv_login, or, forgot, txt_yes;
    private ProgressDialog loadingBar;
    String string;

    Typeface tf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //DECLARE
        //Font
//        tf = Typeface.createFromAsset(getAssets(), "Verdana.ttf");

        email = findViewById(R.id.email);
        email.setTypeface(tf);

        btn_fb = findViewById(R.id.btn_fb);
        btn_fb.setTypeface(tf);

        or = findViewById(R.id.or);
        or.setTypeface(tf);

        txt_yes = findViewById(R.id.txt_yes);
        txt_yes.setTypeface(tf);

        btn_hp = findViewById(R.id.btn_hp);
        btn_hp.setTypeface(tf);

        loadingBar = new ProgressDialog(this);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, Login.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        tv_login = findViewById(R.id.tv_login);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, Login.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

//        btn_hp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Register.this, RegisterPonsel.class);
//                startActivity(i);
//                finish();
//            }
//        });

        btn_regist = findViewById(R.id.btn_regist);
        btn_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, FormRegisterEmail.class);
                string = email.getText().toString();
                i.putExtra("email",string);
                startActivity(i);
                finish();
            }
        });

    }
}